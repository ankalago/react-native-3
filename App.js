import {StatusBar} from 'expo-status-bar';
import React from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
import {createAppContainer, createSwitchNavigator} from 'react-navigation'; // TODO mantiene el estado de la navegación
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {Ionicons} from '@expo/vector-icons';

const Logo = ({title}) => {
    return (
        <Text>{title}</Text>
    )
}

const HomeScreen = ({navigation}) => {
    return (
        <View style={styles.container}>
            <Text>Open up App.js to start working on your app!</Text>
            <Button title='ir a Detalle' onPress={() => navigation.navigate('Detail')}/>
        </View>
    )
}

HomeScreen.navigationOptions = {
    drawerIcon: ({tintColor}) => {
        return <Ionicons name='ios-information-circle' size={25} color={tintColor} />
    },
    title: 'Home Screen',
    headerStyle: {
        backgroundColor: '#ffeecc'
    },
    headerTintColor: '#FF0000',
    headerTitleStyle: {
        fontWeight: 'bold',
        fontSize: 10,
    }
}

const DetailScreen = ({navigation}) => {
    // const [cont, setCont] = useState(0)
    // const increment = () => setCont(cont + 1)
    //
    // useEffect(() => {
    //     navigation.setParams({
    //         increment
    //     })
    // }, [cont])

    const title = navigation.getParam('title', 'valor por defecto')
    return (
        <View style={styles.container}>
            <Text>Detail - {title}</Text>
            <Button title='Regresar' onPress={() => navigation.goBack()}/>
            <Button title='SetParam' onPress={() => navigation.setParams({title: 'SetParam Title'})}/>
            <Button title='ir a Home' onPress={() => navigation.navigate('Home')}/>
            <Button title='ir a Modal' onPress={() => navigation.navigate('Modal')}/>
        </View>
    )
}

DetailScreen.navigationOptions = ({navigation}) => {
    return {
        // title: navigation.getParam('title', 'Detail Screen'),
        headerTitle: (
            <Logo title={navigation.getParam('title', 'Detail Screen')}/>
        ),
        headerRight: (
            <Button
                onPress={() => navigation.getParam('increment')}
                title='Incrementar'
                color='#555'/>
        ),
    }
}

const AppNavigator = createSwitchNavigator({
    Home: {
        screen: HomeScreen
    },
    Detail: {
        screen: DetailScreen
    },
}, {
    initialRouteName: 'Home',
})

const ModalScreen = () => {
    return (
        <View style={styles.modal}>
            <View style={styles.close}>
                <Text style={styles.closeText}>x</Text>
            </View>
            <Text>View</Text>
        </View>
    )
}

const RootStack = createStackNavigator({
    Main: AppNavigator,
    Modal: {
        screen: ModalScreen
    }
}, {
    mode: 'modal',
    headerMode: 'none'
})

export default createAppContainer(RootStack)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    modal: {
        flex: 1,
        backgroundColor: '#fff000',
        alignItems: 'center',
        justifyContent: 'center',
    },
    close: {
        position: 'absolute',
        top: 40,
        right: 20,
        backgroundColor: '#ff0000',
        width: 50,
        height: 50,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
    },
    closeText: {
        fontSize: 30,
        margin: 0,
        padding: 0
    }
});
